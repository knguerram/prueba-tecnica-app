/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import Router from './src/Router/index';
import { Dimensions, ImageBackground } from 'react-native';
import {Provider} from 'react-redux';
import {store} from './src/Redux/store';


const bg = require('./src/assets/images/Fondo.jpg');
const { height, width } = Dimensions.get('window');

const App = () => (
  <Provider store={store}>
    <ImageBackground
      source={bg}
      style={{ flex: 1, height, width }} 
    >
      <Router />
    </ImageBackground>

  </Provider>
);

export default App;
