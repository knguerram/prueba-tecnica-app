import React, {Component} from 'react';
import {View, Text, Dimensions, ImageBackground} from 'react-native';
import axios from 'axios';
import FilterInput from '../Components/Input';

import DataList from '../Components/DataList';

//Vista principal donde los datos son traidos por medio de Api Rest, la cual esta dividida por componentes.
const bg = require('../assets/images/Fondo.jpg');
const { height, width } = Dimensions.get('window');

class ListToDo extends Component {
  state = {
    data: [],
    id: '',
    title: '',
  };
  componentDidMount = () => {
    this.getTodos();
  };
  getTodos = () => {
    const params = {};
    if (this.state.id !== '') {
      params.id = this.state.id;
    }
    if (this.state.title !== '') {
      params.q = this.state.title;
    }
    axios({
      method: 'get',
      url: 'https://jsonplaceholder.typicode.com/todos?',
      params,
    }).then(response => {
      this.setState({data: response.data});
    });
  };
  render() {
    return (
      <ImageBackground
        source={bg}
        style={{ flex: 1, height, width }} 
      >
        <View>
          <FilterInput
            icon="key"
            placeholder="Filtrar por ID"
            value={this.state.id}
            keyboardType="numeric"
            onChangeText={async id => {
              await this.setState({id});
              this.getTodos();
            }}
          />
          <FilterInput
            icon="font"
            placeholder="Filtar por titulo"
            value={this.state.title}
            onChangeText={async title => {
              await this.setState({title});
              this.getTodos();
            }}
          />
          <DataList data={this.state.data} />
        </View>
      </ImageBackground>
    );
  }
}

export default ListToDo;
