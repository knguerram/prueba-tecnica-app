import React from 'react';
import {View, Text, Dimensions, ImageBackground} from 'react-native';
import Button from '../Components/Button';

//Vista principal donde se puede ingresar de dos maneras diferentes tanto por Api rest como por Firebase.
const bg = require('../assets/images/Fondo.jpg');
const { height, width } = Dimensions.get('window');

const SelectView = ({navigation}) => (
  <ImageBackground
  source={bg}
  style={{ flex: 1, height, width }} 
  >
    <View style={{flex: 1, justifyContent: 'space-evenly'}}>
      <Text style={{textAlign: 'center', fontSize: 30, fontWeight: 'bold', color: '#6C6E6F'}}>
        Seleccione una opción
      </Text> 
      <Button
        title="Api Rest"
        type="solid"
        icon="server"
        onPress={() => {
          navigation.navigate({routeName: 'ListToDo'});
        }}
      />
      <Button
        style={{ width: 100 }}
        title="Firebase"
        type="solid"
        icon="fire"
        onPress={() => {
          navigation.navigate({routeName: 'FirebaseTodo'});
        }}
      />
    </View>
  </ImageBackground>
);
export default SelectView;
