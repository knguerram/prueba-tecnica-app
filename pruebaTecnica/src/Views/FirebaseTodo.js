import React, {Component} from 'react';
import {View, Text, Dimensions, ImageBackground} from 'react-native';
import {connect} from 'react-redux';
import Button from '../Components/Button';
import Input from '../Components/Input';
import DataList from '../Components/DataList';
import {getTodos} from '../Redux/Actions/TodosActions';

//Vista Firebase, el cual esta compuesto por componentes: input, la lista de tareas creadas y el button.
const bg = require('../assets/images/Fondo.jpg');
const { height, width } = Dimensions.get('window');

class FirebaseTodos extends Component {
  state = {
    id: '',
    title: '',
  };
  componentDidMount() {
    this.props.getTodos();
  }
  filterQuery = () => {
    this.props.getTodos(this.state.id, this.state.title);
  };
  render() {
    console.log(this.props);
    return (
      <ImageBackground
        source={bg}
        style={{ flex: 1, height, width }} 
      >
        <View style={{flex: 1}}>
          <Input
            icon="key"
            placeholder="Filtrar por ID"
            value={this.state.id}
            onChangeText={async id => {
              await this.setState({id});
              this.filterQuery();
            }}
          />
          <Input
            icon="font"
            placeholder="Filtrar por titulo"
            value={this.state.title}
            onChangeText={async title => {
              await this.setState({title});
              this.filterQuery();
            }}
          />
          <DataList
            data={this.props.todos.todos}
            onPress={item => {
              this.props.navigation.navigate('DetailTodo', {item: item});
            }}
          />
          <Button
            icon="plus"
            title="Nueva tarea"
            onPress={() => {
              this.props.navigation.navigate({routeName: 'CreateTodo'});
            }}
          />
        </View>
      </ImageBackground>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  getTodos: (id, title) => dispatch(getTodos(id, title)),
});

export default connect(
  state => ({todos: state}),
  mapDispatchToProps,
)(FirebaseTodos);
