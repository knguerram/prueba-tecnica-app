import React, {Component} from 'react';
import {View, Alert, Text, Dimensions, ImageBackground} from 'react-native';
import firebase from 'react-native-firebase';
import {connect} from 'react-redux';
import {getTodos} from '../Redux/Actions/TodosActions';
import Input from '../Components/Input';
import Button from '../Components/Button';

//Vista: Creación de tarea
const bg = require('../assets/images/Fondo.jpg');
const { height, width } = Dimensions.get('window');

class CreateTodo extends Component {
  constructor() {
    super();
    this.ref = firebase.firestore().collection('todos');
    this.state = {
      id: '',
      title: '',
    };
  }
  //Función para crear una tarea
  saveTodo = () => {
    this.ref
      .add({
        id: this.state.id,
        title: this.state.title,
      })
      .then(data => {
        this.props.getTodos();
        this.showAlert('Tarea creada correctamente', () => {
          this.props.navigation.navigate('FirebaseTodo');
        });
      })
      .catch(() => {
        this.showAlert('Error no se a podido crear la tarea', () => {});
      });
  };
  //Se crea una alerta para indicarle que la tarea se creo correctamente o si hay algun error al crearlo
  showAlert = (message, onPress) => {
    Alert.alert(
      'Agregar Tarea',
      message,
      [
        {
          text: 'Cancel',
          onPress: () => {},
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: () => {
            onPress();
          },
        },
      ],
      {cancelable: false},
    );
  };
  render() {
    return (
      <ImageBackground
        source={bg}
        style={{ flex: 1, height, width }} 
      >
        <View style={{flex: 1, justifyContent: 'center'}}>
          <Input
            icon="key"
            placeholder="ID"
            value={this.state.id}
            onChangeText={id => {
              this.setState({id});
            }}
          />
          <Input
            icon="plus"
            placeholder="Title"
            value={this.state.title}
            onChangeText={title => {
              this.setState({title});
            }}
          />
          <Button icon="save" title="Guardar" onPress={this.saveTodo} />
        </View>
      </ImageBackground>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  getTodos: () => dispatch(getTodos()),
});

export default connect(
  null,
  mapDispatchToProps,
)(CreateTodo);
