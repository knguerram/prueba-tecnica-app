import React, {useState, useEffect} from 'react';
import {View, Text, Alert, Dimensions, ImageBackground} from 'react-native';
import firebase from 'react-native-firebase';
import {connect} from 'react-redux';
import Input from '../Components/Input';
import Button from '../Components/Button';
import {getTodos} from '../Redux/Actions/TodosActions';

const db = firebase.firestore().collection('todos');
const bg = require('../assets/images/Fondo.jpg');
const { height, width } = Dimensions.get('window');

//Vista: Editar y/o elimnar una tarea

const handleDelate = (document_id, navigation, getTodos) => {
  Alert.alert(
    'Eliminar Tarea? ',
    'Esta sguro de eliminar la tarea?',
    [
      {
        text: 'Cancelar',
        onPress: () => {},
        style: 'cancel',
      },
      {
        text: 'OK',
        onPress: () => {
          db.doc(document_id)
            .delete()
            .then(() => {
              getTodos();
              navigation.navigate('FirebaseTodo');
            });
        },
      },
    ],
    {cancelable: false},
  );
};
const handleEdit = (document_id, id, title, getTodos) => {
  db.doc(document_id)
    .update({id, title})
    .then(() => {
      getTodos();
    });
};
const DetailTodo = ({navigation, getTodos}) => {
  const [id, setId] = useState('');
  const [title, setTitle] = useState('');
  const [edit, setEdit] = useState(false);
  const [document_id, setDocument] = useState('');
  useEffect(() => {
    setId(navigation.state.params.item.id.toString());
    setTitle(navigation.state.params.item.title);
    setDocument(navigation.state.params.item.document_id);
  }, [navigation.state.params]);

  console.log(navigation);
  return (
    <ImageBackground
      source={bg}
      style={{ flex: 1, height, width }} 
    >
      <View>
        <Input
          icon="key"
          value={id}
          onChangeText={text => {
            setId(text);
          }}
          editable={edit}
        />
        <Input
          icon="font"
          value={title}
          onChangeText={text => {
            setTitle(text);
          }}
          editable={edit}
        />
        <Button
          title={edit ? 'Guardar' : 'Editar'}
          onPress={() => {
            if (edit) {
              handleEdit(document_id, id, title, getTodos);
              setEdit(false);
            } else {
              setEdit(true);
            }
          }}
          containerStyle={{marginTop: 20, marginBottom: 5}}
        />
        <Button
          title="Eliminar"
          color="#f44336"
          containerStyle={{borderRadius: 30, marginTop: 5}}
          onPress={() => handleDelate(document_id, navigation, getTodos)}
        />
      </View>
    </ImageBackground>
  );
};

const mapDispatchToProps = dispatch => ({
  getTodos: () => dispatch(getTodos()),
});

export default connect(
  null,
  mapDispatchToProps,
)(DetailTodo);
