import {createStackNavigator, createAppContainer} from 'react-navigation';
import ListToDo from '../Views/ListToDo';
import SelectView from '../Views/SelectView';
import FirebaseTodo from '../Views/FirebaseTodo';
import CreateTodo from '../Views/CreateTodo';
import DetailTodo from '../Views/DetailTodo';

//Navegacion entre pantallas

const MainNavigator = createStackNavigator({
  SelectView: {screen: SelectView, navigationOptions: { title: 'Bienvenidos'}},
  ListToDo: {screen: ListToDo, navigationOptions: { title: 'Bienvenidos'}},
  FirebaseTodo: {screen: FirebaseTodo, navigationOptions: { title: 'Lista de Tareas'}},
  CreateTodo: {screen: CreateTodo, navigationOptions: { title: 'Creación de Tarea'}},
  DetailTodo: {screen: DetailTodo, navigationOptions: { title: 'Modifición o eliminación de Tarea'}},
},{
  initialRouteName: 'SelectView',
  navigationOptions: {
    title: 'Prueba Técnica',
    headerTitleAllowFontScaling: true,
    headerBackTitle: 'Atras',
    gesturesEnabled: true,
  }

});

const Router = createAppContainer(MainNavigator);

export default Router;
