import React from 'react';
import {
  Text,
  FlatList,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native';
import {Card} from 'react-native-elements';

//Listado de las tareas creadas

const RenderCard = ({item, onPress}) => (
  <TouchableOpacity
    onPress={
      onPress
        ? () => {
            onPress(item);
          }
        : () => {}
    }>
    <Card containerStyle={{elevation: 3, borderWidth: 0, borderRadius: 30}}>
      <Text>ID: {item.id}</Text>
      <Text>Title: {item.title}</Text>
    </Card>
  </TouchableOpacity>
);

const DataList = ({data, onPress}) => {
  return (
    <FlatList
      data={data}
      keyExtractor={(item, index) => {
        return index;
      }}
      renderItem={({item}) => <RenderCard item={item} onPress={onPress} />}
      ListEmptyComponent={() => (
        <ActivityIndicator size="large" color="blue" style={{marginTop: 50}} />
      )}
    />
  );
};

export default DataList;
