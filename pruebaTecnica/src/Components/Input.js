import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Input} from 'react-native-elements';

const FilterInput = ({icon, ...rest}) => (
  <Input
    containerStyle={{marginTop: 10, marginBottom: 10}}
    leftIconContainerStyle={{marginRight: 10}}
    inputContainerStyle={{borderWidth: 0}}
    leftIcon={<Icon name={icon} size={16} color="black" />}
    {...rest}
  />
);

export default FilterInput;
