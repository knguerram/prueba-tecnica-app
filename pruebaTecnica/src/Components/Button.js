import React from 'react';
import {Button} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
const ButtonCustom = ({icon, color, ...rest}) => {
  return (
    <Button
      icon={
        <Icon name={icon} size={15} color="white" style={{marginRight: 10}} />
      }
      iconLeft
      buttonStyle={!color ? {height: 50, backgroundColor: '#53C3D3', borderRadius: 30} : {backgroundColor: color, height: 50}}
      {...rest}
    />
  );
};

export default ButtonCustom;
