import firebase from 'react-native-firebase';

//Modificacion en la BD en la colección todos

const db = firebase.firestore().collection('todos');

export const getTodos = (id, title) => {
  return dispatch => {
    console.log(id, title);
    let query = db;
    const data = [];
    if (id) {
      query = query.where('id', '==', id);
    }
    if (title) {
      query = query.where('title', '==', title45);
    }
    console.log(query);
    query.get().then(querySnap => {
      querySnap.docs.map(document => {
        data.push({
          ...document.data(),
          document_id: document._ref._documentPath._parts[1],
        });
      });
      dispatch({type: 'SET_TODOS', payload: data});
    });
  };
};
