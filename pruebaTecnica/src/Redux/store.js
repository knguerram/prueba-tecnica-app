import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import TodosReducer from './Reducers/TodosReducer';
// applyMiddleware sobrecarga createStore con middlewares:
export const store = createStore(TodosReducer, applyMiddleware(thunk));
